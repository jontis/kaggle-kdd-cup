#!/usr/bin/env python3


import os, glob, pickle, tables
import numpy as np
import pandas as pd
#from progressbar import ProgressBar, Percentage, Bar, RotatingMarker, ETA
from joblib import Parallel, delayed, Memory
import joblib
from math import floor, ceil
import matplotlib.pyplot as pyplot
from sklearn import cross_validation, ensemble, preprocessing, metrics
import matplotlib.pyplot

class dataObject(object):
    def __init__(self):
        self.paths = {'data_gen': '/home/jonathan/kaggle/kddCup2014/dataGen/',
                      'data_source': '/home/jonathan/kaggle/kddCup2014/dataSource/'}
        
    def _ReadFile(self, name, columns_get=None):
        file_name = self.paths['data_source'] + name + '.csv'
        #if verbose: print(name)
        data = pd.read_csv(file_name, usecols=columns_get)
        #fix the column names
        column_names = data.columns.tolist()
        return data
    
    def _ReadSource(self):
        self.outcomes = self._ReadFile('outcomes')
        self.projects = self._ReadFile('projects')
        self.essays = self._ReadFile('essays')
        self.submission = self._ReadFile('sampleSubmission')
    
    
    def strings_to_unique_num(self, df):
        #TODO: this needs to be processed for the submission set also
        for col in df.columns:
            print(col)
            if df[col].dtype == np.dtype('O'):
                df[col] =  np.unique(df[col].fillna(value='na'), return_inverse=True)[1] # NaN replaced with na or np.unique stumbles
        return df
    
    
    def DataNumeric():
        """empty def, only serve numeric data"""


    def DataNonNum():
        """empty def, only serve non-numeric data"""


def bool_char_to_num(df):
    for col in df.columns:
        if (set(df[col][:10]) | set(['t', 'f'])) == set(['t', 'f']): # precheck, the full check is really slow
            if (set(df[col]) | set(['t', 'f'])) == set(['t', 'f']):
                df[col] = df[col].apply(lambda x:0 if x =='f' else 1)
    return df


def encode_proj_data(df, le_list=None):
    print('encoding label data')
    # useful for classifying all non-num data
    # lv can contain a list of prefit transforms
    out_array = np.zeros(df.shape, np.dtype('float32'))
    out_col = list()
    if le_list != None: 
        assert len(le_list) == len(df.columns)
        for i_col, le in enumerate(le_list):
            if le is not None:
                out_array[:, i_col] = le.transform(df.iloc[:, i_col].fillna(value='na'))
            else:
                out_array[:, i_col] = df.iloc[:, i_col].fillna(value=0)
        return out_array
    else:
        le_list = list()
        for i_col, col in enumerate(df.columns):
            # print(i_col, col)
            if df[col].dtype == np.dtype('O'):
                le_list.append(preprocessing.LabelEncoder())
                le_list[-1].fit(df[col].fillna(value='na'))
                out_array[:, i_col] = le_list[-1].transform(df[col].fillna(value='na'))
            else:
                le_list.append(None)
                out_array[:, i_col] = df[col].fillna(value=0)
        return out_array, le_list


def worst_feature_selector(X, y, feature_names, verbose=False):
    auc = []
    kf = cross_validation.KFold(X.shape[0], shuffle=True)
    train_index, test_index = kf.__iter__().__next__()
    for i, feature in enumerate(feature_names):
        if verbose: print(feature)
        if X.shape[1] > 1:
            X_less = np.delete(X, i, 1)
        else:
            X_less = X
    
        rf = ensemble.RandomForestClassifier(n_estimators=50, n_jobs=2, verbose=verbose, min_samples_leaf=20, bootstrap=False, min_samples_split=200)
        assert(type(y) == numpy.ndarray)
        rf.fit(X_less[train_index, :], y[train_index])
        pred = rf.predict_proba(X_less[test_index, :])
        auc.append(metrics.roc_auc_score(y[test_index], pred[:, 1]))
        if verbose: print(auc)
    auc = np.array(auc)
    return feature_names[auc.argmax()], auc.max()        


def recursive_feature_eliminator(X, y, feature_names, removed_columns=None):
    if removed_columns is None: removed_columns = []
    auc = []
    remaining_features = feature_names.drop(feature_names[removed_columns])
    print(remaining_features)
    while len(remaining_features) > 0:
        X_reduced = np.delete(X, removed_columns, 1)
        worst_feature, best_auc = worst_feature_selector(X_reduced, y, remaining_features)
        removed_columns.append(feature_names.get_loc(worst_feature))
        remaining_features = remaining_features.drop(worst_feature)
        auc.append(best_auc)
        print(worst_feature)
        print(auc)
    removed_features = feature_names[removed_columns]
    return removed_features, auc
            

def count_essay_length(df):
    out = pd.DataFrame(df.projectid)
    out['essay_length'] = 0
    for i in range(df.shape[0]):
        try:
            out.essay_length[i] = len(df.essay[i])
        except:
            out.essay_length[i] = 0
    return out


def split_date_to_year_month(df):
    # split date to year month
    out = []
    date = df.date_posted
    for i in date:
        out.append(i.split('-'))
    out = pd.DataFrame(out, dtype=np.int64)
    out.columns = ['year', 'month', 'day']
    return out


# load and refine data
data = dataObject()
data._ReadSource()
data.essay_length = count_essay_length(data.essays)

# prune and merge data
data.projects = pd.merge(data.projects, data.outcomes.ix[:,['projectid', 'is_exciting']], on='projectid', how='outer')
data.projects.is_exciting = data.projects.is_exciting.fillna(value='f')
data.projects = pd.merge(data.projects, data.essay_length, on='projectid', how='outer')
data.projects = bool_char_to_num(data.projects)
data.projects = pd.concat([data.projects, split_date_to_year_month(data.projects)], axis=1)
project_ids = data.projects.projectid
data.projects.drop('projectid', axis=1, inplace=True)


# manual prune, not much interesting after this
#data.projects = data.projects[:470000]

# drop preselect unimportant
drop_feats = ['teacher_teach_for_america', 'teacher_prefix', 'school_district',
     'school_nlns', 'primary_focus_subject', 'secondary_focus_area', 'school_state',
     'grade_level', 'eligible_almost_home_match', 'fulfillment_labor_materials',
     'school_ncesid', 'school_metro', 'school_charter', 'school_magnet',
     'secondary_focus_subject', 'primary_focus_area', 'school_year_round',
     'school_city', 'school_county', 'poverty_level', 'eligible_double_your_impact_match',
     'resource_type', 'school_kipp', 'school_charter_ready_promise',
     'teacher_ny_teaching_fellow', 'total_price_including_optional_support', 'date_posted']
drop_feats = ['school_kipp', 'school_city', 'school_year_round', 'grade_level',
             'schoolid', 'school_metro', 'resource_type', 'total_price_including_optional_support',
             'teacher_acctid', 'essay_length', 'teacher_prefix', 'school_zip', 
             'secondary_focus_subject', 'school_nlns', 'school_magnet', 
             'primary_focus_subject', 'secondary_focus_area', 'school_district', 
             'school_state', 'fulfillment_labor_materials', 'school_charter_ready_promise', 
             'school_charter', 'eligible_almost_home_match',  'date_posted',
             'poverty_level','teacher_ny_teaching_fellow', 'day']
#data.projects.drop(drop_feats, axis=1, inplace=True)


# split data
is_exciting = data.projects.is_exciting
data.projects.drop('is_exciting', axis=1, inplace=True)


# encode data
data.proj_out_enc, encoders_proj_data = encode_proj_data(data.projects)


# select train data
data.train_data_index = (data.projects.year == 2013)
data.train_X = data.proj_out_enc[data.train_data_index.values, :]
data.train_y = is_exciting[data.train_data_index.values].values

# select sub data
data.sub_data_index = (data.projects.year == 2014)
data.sub_X = data.proj_out_enc[data.sub_data_index.values, :]


# pull test set from last year of data
kf = cross_validation.KFold(data.train_X.shape[0], shuffle=True)

#aaa, bbb = recursive_feature_eliminator(data.train_X, data.train_y, data.projects.columns)
#pyplot.plot(bbb)


auc = []
predlist = []
truthlist = []
for train_index, test_index in kf:
    rf = ensemble.RandomForestClassifier(n_estimators=100, n_jobs=2, verbose=True, min_samples_leaf=20, bootstrap=False, min_samples_split=200)
    rf.fit(data.train_X[train_index, :], data.train_y[train_index])
    predlist.append(rf.predict_proba(data.train_X[test_index, :]))
    pred = rf.predict_proba(data.train_X[test_index, :])
    auc.append(metrics.roc_auc_score(data.train_y[test_index], pred[:, 1]))
    truthlist.append(data.train_y[test_index])
    print(auc)

# full data set for submission
rf = ensemble.RandomForestClassifier(n_estimators=100, n_jobs=2, verbose=True, min_samples_leaf=20, bootstrap=False, min_samples_split=200)
rf.fit(data.train_X, data.train_y)
pred = rf.predict_proba(data.sub_X )

submission = pd.DataFrame.from_dict({'projectid': project_ids[:data.sub_X.shape[0]], 'is_exciting': pred[:, 1]})
submission.sort(columns='projectid', ascending=False, inplace=True)
submission = submission.reindex_axis(['projectid', 'is_exciting'], axis=1)
#submission.to_csv('submission1.csv', index=False)


#data.LoadFromSource()
#aaa = bucketizedReturnsFeatureSet(data)
#date_start = np.datetime64('2012-02-01')
#date_stop = np.datetime64('2012-02-20')
#buckets = np.array([2,2,2])
#memory = buckets.sum()
#data_s = aaa._CropDates(aaa.source_data.data, date_start, date_stop)
## NaN handling?
#markers = data_s.data.columns
