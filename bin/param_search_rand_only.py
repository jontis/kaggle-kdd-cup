"""
randomized search
"""

import numpy as np

from time import time
from operator import itemgetter
from scipy.stats import randint as sp_randint

from sklearn.grid_search import RandomizedSearchCV, GridSearchCV
from sklearn.datasets import load_digits
from sklearn.ensemble import RandomForestClassifier

# get some data
X = np.load('x.npy')
y = np.load('y.npy')

# build a classifier
clf = RandomForestClassifier(n_estimators=50, n_jobs=2)


# Utility function to report best scores
def report(grid_scores, n_top=10):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))


# specify parameters and distributions to sample from
param_dist = {"min_samples_split": [200],
              "min_samples_leaf": [20],
              "bootstrap": [False],
              "n_estimators": [10, 50, 100, 500]}

# run randomized search
n_iter_search = 20
random_search = GridSearchCV(clf, param_grid=param_dist, scoring='roc_auc',
                                   verbose=100, n_jobs=2)

start = time()
random_search.fit(X, y)
print("RandomizedSearchCV took %.2f seconds for %d candidates"
      " parameter settings." % ((time() - start), n_iter_search))
report(random_search.grid_scores_)
