#!/usr/bin/env python3


import os, glob, pickle, tables
import numpy as np
import pandas as pd
#from progressbar import ProgressBar, Percentage, Bar, RotatingMarker, ETA
from joblib import Parallel, delayed, Memory
import joblib
from math import floor, ceil
import matplotlib.pyplot as pyplot
from sklearn import cross_validation, ensemble, preprocessing, metrics

class dataObject(object):
    def __init__(self):
        self.paths = {'data_gen': '/home/jonathan/kaggle/kddCup2014/dataGen/',
                      'data_source': '/home/jonathan/kaggle/kddCup2014/dataSource/'}
        
    def _ReadFile(self, name, columns_get=None):
        file_name = self.paths['data_source'] + name + '.csv'
        #if verbose: print(name)
        data = pd.read_csv(file_name, usecols=columns_get)
        #fix the column names
        column_names = data.columns.tolist()
        return data
    
    def _ReadSource(self):
        self.outcomes = self._ReadFile('outcomes')
        self.projects = self._ReadFile('projects')

    
    def bool_char_to_num(self, df):
        for col in df.columns:
            if (set(df[col][:10]) | set(['t', 'f'])) == set(['t', 'f']): # precheck, the full check is really slow
                if (set(df[col]) | set(['t', 'f'])) == set(['t', 'f']):
                    df[col] = df[col].apply(lambda x:0 if x =='f' else 1)
        return df
    
    
    def strings_to_unique_num(self, df):
        #TODO: this needs to be processed for the submission set also
        for col in df.columns:
            print(col)
            if df[col].dtype == np.dtype('O'):
                df[col] =  np.unique(df[col].fillna(value='na'), return_inverse=True)[1] # NaN replaced with na or np.unique stumbles
        return df
    
    
    def DataNumeric():
        """empty def, only serve numeric data"""


    def DataNonNum():
        """empty def, only serve non-numeric data"""



data = dataObject()
data._ReadSource()
data.projects_with_outcome = data.projects[data.projects.projectid.isin(data.outcomes.projectid)].copy()

data.projects_with_outcome = data.bool_char_to_num(data.projects_with_outcome)
data.outcomes = data.bool_char_to_num(data.outcomes)
data.projects_with_outcome = data.strings_to_unique_num(data.projects_with_outcome)
#data.outcomes = data.strings_to_unique_num(data.outcomes)
data.outcomes.sort(columns='projectid', inplace=True)
data.projects_with_outcome.sort(columns='projectid', inplace=True)

#data.projects_with_outcome = impute.fit_transform(data.projects_with_outcome)


project_ids = set(data.outcomes.projectid)
data.projects_with_outcome.drop('date_posted', axis=1, inplace=True) # drop column date now as it crashes RF
kf = cross_validation.KFold(len(project_ids), shuffle=True)

auc = []
for train_index, test_index in kf:
    rf = ensemble.RandomForestClassifier()
    rf.fit(data.projects_with_outcome.fillna(value=0).values[train_index, :], data.outcomes.iloc[train_index, :]['is_exciting'])
    pred = rf.predict_proba(data.projects_with_outcome.fillna(value=0).values[test_index, :])
    auc.append(metrics.roc_auc_score(data.outcomes.iloc[test_index, :]['is_exciting'], pred[:, 1]))
    print(auc)

#data.LoadFromSource()
#aaa = bucketizedReturnsFeatureSet(data)
#date_start = np.datetime64('2012-02-01')
#date_stop = np.datetime64('2012-02-20')
#buckets = np.array([2,2,2])
#memory = buckets.sum()
#data_s = aaa._CropDates(aaa.source_data.data, date_start, date_stop)
## NaN handling?
#markers = data_s.data.columns
