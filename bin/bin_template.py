#!/usr/bin/env python3


import os, glob, pickle, tables
import numpy as np
import pandas as pd
#from progressbar import ProgressBar, Percentage, Bar, RotatingMarker, ETA
from joblib import Parallel, delayed, Memory
import joblib
from math import floor, ceil
import matplotlib.pyplot as pyplot
from sklearn import cross_validation

class dataObject(object):
    def __init__(self):
        self.paths = {'data_gen': '/home/jonathan/kaggle/kddCup2014/dataGen',
                      'data_source': '/home/jonathan/kaggle/kddCup2014/dataSource'}
        
    def _ReadFile(self, name, columns_get=None):
        file_name = self.paths.data_gen + name + '.csv'
        if verbose: print(name)
        data = pd.read_csv(file_name, usecols=columns_get)
        #fix the column names
        column_names = data.columns.tolist()
        return data
    
    def _ReadSource(self):
        self.outcomes = self._ReadFile('outcomes')
        self.projects = self._ReadFile('outcomes')
    
    def DataNumeric():
        """empty def, only serve numeric data"""


    def DataNonNum():
        """empty def, only serve non-numeric data"""





data = dataObject()
data._ReadSource()
#kf = cross_validation.k

#data.LoadFromSource()
#aaa = bucketizedReturnsFeatureSet(data)
#date_start = np.datetime64('2012-02-01')
#date_stop = np.datetime64('2012-02-20')
#buckets = np.array([2,2,2])
#memory = buckets.sum()
#data_s = aaa._CropDates(aaa.source_data.data, date_start, date_stop)
## NaN handling?
#markers = data_s.data.columns
